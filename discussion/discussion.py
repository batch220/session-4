class Car():
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		self.fuel = "Gasoline"
		self.fuel_level = 0

	#methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print(f"Filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

new_car = Car("Honda", "Jazz", 2020)
print(f"My car is a {new_car.brand} {new_car.model} {new_car.year_of_make}")

#[SECTION] Encapsulation
# a form of data hiding in Python
class Person():
	def __init__(self):
		self._name = "John Doe"
		self._age = 0

	def set_name(self,name):
		self._name = name
	def get_name(self):
		print(f"Name of Person {self._name}")

	def set_age(self,age):
		self._age = age
	def get_age(self):
		print(f"Age of Person {self._age}")

p1 = Person()
p1.get_name()
p1.set_name("Jane Doe")
p1.get_name()

p1.get_age()
p1.set_age(13)
p1.get_age()

"""
Mini-activity
-add another protected attribute called age
"""

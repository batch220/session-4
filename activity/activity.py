# Activity:
# Create a folder called activity and inside the activity, a file called activity.py

# Create the application to solve the following problems:

# Specifications:
# Specification
# 1. Create an abstract class called Animal that has the following abstract methods
# Abstract Methods: 
# * eat(food)
# * make_sound()

# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:

# Properties:
# * name
# * breed
# * age

# Methods:
# * getters and setters
# * implementation of abstract methods
# * call()

# After completing the activity, push the activity solution to gitlab and paste the link to the repository in Boodle

from abc import ABC, abstractmethod

class Animal(ABC):

     # @abstractmethod
     # def eat(self):
     #    pass
     @abstractmethod
     def make_sound(self):
        pass
   

class Dog(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    #getters
    def get_name(self):
        print(f"Animal's name is: {self.name}")
    def get_breed(self):
        print(f"Animal's breed is: {self.breed}")
    def get_age(self):
        print(f"Animal's age is: {self.age}")
     
    #setters
    def set_name(self):
        self.name = name
    def set_breed(self):
        self.breed = breed
    def set_age(self):
        self.breed = breed

    #methods   
    def make_sound(self):
        print("Bark! Wolf! Arrf!")
    
    def call(self):
        print(f"Here {self.name} is")


class Cat(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    #getters
    def get_name(self):
        print(f"Animal's name is: {self.name}")
    def get_breed(self):
        print(f"Animal's breed is: {self.breed}")
    def get_age(self):
        print(f"Animal's age is: {self.age}")
     
    #setters
    def set_name(self):
        self.name = name
    def set_breed(self):
        self.breed = breed
    def set_age(self):
        self.breed = breed

    #methods   
    def make_sound(self):
        print("Miaow! Nyaw! Nyaaa!")
    
    def call(self):
        print(f"{self.name} Come on")


#Test Codes
dog1 = Dog("Isis","Dalmatian", 15)
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss","Persian", 4)
cat1.make_sound()
cat1.call()




